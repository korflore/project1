Title: DODGEBALL!

![Screenshot](/Screenshot.png)

I wanted to really challenge myself with coding a game in a language that I am not totally familiar with. I also wanted to make something fun rather than just creative. I think games allow me to be a little creative and also fun at the same time. Its the best way I can express my creativity I feel. I did run into a lot of issues with this but overall, I think I did somewhat what I wanted. You can change the difficulty and the ways in which things spawn.
