#pragma once

#include "ofMain.h"
#include "ofxFaceTracker2.h"
#include "ofxBox2d.h"
#include "ofxPanel.h"

#define N_SOUNDS 5

class SoundData {
public:
    int     soundID;
    bool bHit;
};

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    ofxFaceTracker2 tracker;
    ofVideoGrabber grabber;
    ofxPanel gui;
    ofxIntSlider difficulty;
    ofxIntSlider axis;
    ofxBox2d box2d;
    
    int counter=0;
    int timer = 0;
    bool endGame;
    bool faceDetected;
    bool powerActive;
    bool power;
    int rand;
    glm::vec2 leftBound; 
    glm::vec2 rightBound;
    glm::vec2 topBound;
    glm::vec2 botBound;
    ofSoundPlayer  sound[N_SOUNDS];
    
    // this is the function for contacts
    void contactStart(ofxBox2dContactArgs &e);
    //void contactEnd(ofxBox2dContactArgs &e);
    void gameOver();
    void powerUp();


    vector        <shared_ptr<ofxBox2dCircle> >    circles;
    vector        <shared_ptr<ofxBox2dCircle> >    circlesX;
    vector        <shared_ptr<ofxBox2dRect> >    powers;
    vector        <shared_ptr<ofxBox2dRect> > face;
    

    ofxBox2dRect rectangle;
};
