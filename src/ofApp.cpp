#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //initialize variables to false
    faceDetected = false;
    powerActive = false;
    endGame = false;
    power = false;
    
    //gui setup
    gui.setup();
    gui.add(difficulty.setup("Difficulty", 1, 1, 4));
    gui.add(axis.setup("Objects Flying Directions (3 = X/Y, 2 = X only, 1 = Y only)", 3, 1, 3));
    
    //setup box2d
    box2d.init();
    box2d.enableEvents();   // <-- turn on the event listener
    box2d.setFPS(60.0);
    
    // register the listener so that we get the events
    ofAddListener(box2d.contactStartEvents, this, &ofApp::contactStart);
    
    
    // Setup grabber
    grabber.setup(1280,720);
    
    // Setup tracker
    tracker.setup();
    
    
    
}

//Listens for collisions
void ofApp::contactStart(ofxBox2dContactArgs &e) {
    if(e.a != NULL && e.b != NULL) {
        //If a square collides with a circle
        if(e.a->GetType() == b2Shape::e_polygon && e.b->GetType() == b2Shape::e_circle) {
            
            SoundData * aData = (SoundData*)e.a->GetBody()->GetUserData();
            SoundData * bData = (SoundData*)e.b->GetBody()->GetUserData();
            
            
            if(aData) {
                
                aData->bHit = true;
                endGame = true;
                //gameOver(e);
                
            }
            
        }
        //if a square collides with a square
        if(e.a->GetType() == b2Shape::e_polygon && e.b->GetType() == b2Shape::e_polygon ) {
            
            SoundData * aData = (SoundData*)e.a->GetBody()->GetUserData();
            SoundData * bData = (SoundData*)e.b->GetBody()->GetUserData();
            
            
            if(aData) {
                aData->bHit = true;
                power = true;
                
            }
        }
        
    }
}

//Powerup method that deletes all the objects from the vectors they are held in
//Then activates the powerActive boolean
void ofApp::powerUp(){
    
    
    rand = ofRandom(1,3);
    timer = counter+300;
    powerActive = true;
    circles.clear();
    circlesX.clear();
    powers.clear();
    rectangle.setPosition(0, 0);
    power = false;
}

//Powerup method that deletes all the objects from the vectors they are held in
//Then resets the score
void ofApp::gameOver(){
    
    
    counter = 0;
    circles.clear();
    circlesX.clear();
    powers.clear();
    rectangle.setPosition(0, 0);
    powerActive = false;
    endGame = false;
    
}

void ofApp::update(){
    grabber.update();
    box2d.update();
    //Checks if counter == timer for the pause timer
    //powerup.
    if(counter == timer){
        box2d.enableEvents();
        powerActive = false;
    }
    //Callback functions for gameover and powerups
    if(endGame == true){
        gameOver();
    }
    if(power == true){
        powerUp();
    }
    
    //Grabs our facetracking information and puts them in variables.
    for (auto face : tracker.getInstances()){
        ofxFaceTracker2Landmarks markers = face.getLandmarks();
        
        leftBound = markers.getImagePoint(0); //get the points for the top center of each eye
        rightBound = markers.getImagePoint(16);
        topBound = markers.getImagePoint(8); //get the points for the top center of each eye
        botBound = markers.getImagePoint(19);
    }
    
    //Callback to assign the rectangle to the face for hit detection on the face
    if(faceDetected == false && leftBound.x != 0 && powerActive == false){
        rectangle.setPhysics(0, 0, 0);
        rectangle.setup(box2d.getWorld(), -10000, 0, rightBound.x - leftBound.x, (topBound.y - botBound.y) + 100);
        rectangle.setData(new SoundData());
        faceDetected = true;
        //keeps counter going if you pick the 1st powerup
    }else if(powerActive && rand == 1){
        counter++;
        //keeps counter going if you pick the 2nd powerup and
        //makes the balls smaller
    }else if(powerActive && rand == 2){
        if(counter % 100/difficulty == 0 && counter != 0 && (axis == 3 || axis == 1)){
            auto c = std::make_shared<ofxBox2dCircle>();
            c->setPhysics(1, 0, 0);
            c->setup(box2d.getWorld(), ofRandom(100,1200), ofRandom(0), 10);
            c->setData(new SoundData());
            auto * sd = (SoundData*)c->getData();
            sd->soundID = ofRandom(0, N_SOUNDS);
            sd->bHit    = false;
            circles.push_back(c);
        }
        if(counter % 240 == 0 && counter != 0 && (axis == 3 || axis == 2)){
            
            auto c1 = std::make_shared<ofxBox2dCircle>();
            c1->enableGravity(false);
            c1->setPhysics(1, 0, 0);
            if(counter % 480 == 0 && counter != 0){
                c1->setup(box2d.getWorld(), 1280, ofRandom(0,200), 10);
                c1->setVelocity(-15, 0);
            }else{
                c1->setup(box2d.getWorld(), 0, ofRandom(400,640), 10);
                c1->setVelocity(15, 0);
            }
            c1->setData(new SoundData());
            auto * sd = (SoundData*)c1->getData();
            sd->soundID = ofRandom(0, N_SOUNDS);
            sd->bHit    = false;
            circlesX.push_back(c1);
        }
        counter++;
        //Keeps counter going for power up 3.
    }else if(powerActive && rand == 3){
        
        counter++;
    }
    //Regular game logic to spawn objects left and right and down.
    else{
        rectangle.setPosition(leftBound.x + (rightBound.x - leftBound.x)/2, botBound.y + ((topBound.y - botBound.y)/2) - 50);
        
        if(counter % 100 == 0 && counter != 0){
            auto p = std::make_shared<ofxBox2dRect>();
            p->setPhysics(1, 0.5, 0.9);
            p->setup(box2d.getWorld(), ofRandom(100,1200), ofRandom(0), 40,40);
            p->setData(new SoundData());
            auto * sd = (SoundData*)p->getData();
            sd->soundID = ofRandom(0, N_SOUNDS);
            sd->bHit    = false;
            powers.push_back(p);
        }
        if(counter % 100/difficulty == 0 && counter != 0 && (axis == 3 || axis == 1)){
            auto c = std::make_shared<ofxBox2dCircle>();
            c->setPhysics(1, 0, 0);
            c->setup(box2d.getWorld(), ofRandom(100,1200), ofRandom(0), 20);
            c->setData(new SoundData());
            auto * sd = (SoundData*)c->getData();
            sd->soundID = ofRandom(0, N_SOUNDS);
            sd->bHit    = false;
            circles.push_back(c);
        }
        if(counter % 240 == 0 && counter != 0 && (axis == 3 || axis == 2)){
            
            auto c1 = std::make_shared<ofxBox2dCircle>();
            c1->enableGravity(false);
            c1->setPhysics(1, 0, 0);
            if(counter % 480 == 0 && counter != 0){
                c1->setup(box2d.getWorld(), 1280, ofRandom(0,200), 20);
                c1->setVelocity(-15, 0);
            }else{
                c1->setup(box2d.getWorld(), 0, ofRandom(400,640), 20);
                c1->setVelocity(15, 0);
            }
            c1->setData(new SoundData());
            auto * sd = (SoundData*)c1->getData();
            sd->soundID = ofRandom(0, N_SOUNDS);
            sd->bHit    = false;
            circlesX.push_back(c1);
        }
        counter++;
    }
    
    
    
    // Update tracker when there are new frames
    if(grabber.isFrameNew()){
        tracker.update(grabber);
    }
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    // Draw camera image
    grabber.draw(0, 0);
    
    // Draw text UI
    ofDrawBitmapStringHighlight("Score : "+ofToString(counter), 1000, 20);
    
    //Draws the vertical balls onto the screen
    for(size_t i=0; i<circles.size(); i++) {
        ofFill();
        SoundData * data = (SoundData*)circles[i].get()->getData();
        
        if(data && data->bHit)
            ofSetHexColor(0xff0000);
        else ofSetHexColor(0x4ccae9);
        circles[i].get()->draw();
    }
    //Draws the horizontal balls onto the screen
    for(size_t i=0; i<circlesX.size(); i++) {
        ofFill();
        SoundData * data = (SoundData*)circles[i].get()->getData();
        
        if(data && data->bHit)
            ofSetHexColor(0xff0000);
        else ofSetHexColor(0x4ccae9);
        circlesX[i].get()->draw();
    }
    //Draws the vertical powerups onto the screen
    for(size_t i=0; i<powers.size(); i++) {
        ofFill();
        SoundData * data = (SoundData*)powers[i].get()->getData();
        
        if(data && data->bHit)
            ofSetHexColor(0xff0000);
        else ofSetHexColor(0x4ccae9);
        powers[i].get()->draw();
    }
    
    //Disables the alpha color on the rectangle drawing over the face
    //then draws the rectangle.
    ofEnableAlphaBlending();
    ofSetColor(255,0,0,0);
    rectangle.draw();
    ofDisableAlphaBlending();
    
    ofSetHexColor(0x444342);
    gui.draw();
    
    //When a powerup is active, display things on the screen.
    if(powerActive && rand ==1){
        ofDrawBitmapStringHighlight("Pause Time Power Up Active For : "+ofToString(timer - counter), 640, 400);
    }else if(powerActive && rand ==2){
        ofDrawBitmapStringHighlight("Ball Size Decreased. Active For :  "+ofToString(timer - counter), 640, 400);
    }else if(powerActive && rand ==3){
        ofSetColor(255, 0, 0, 1);
        ofDrawBitmapStringHighlight("HitBox Shape activated : "+ofToString(timer - counter), 640, 400);
    }
}

